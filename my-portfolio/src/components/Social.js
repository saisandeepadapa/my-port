import React, { Component } from 'react';
class Social extends Component {
    render() {
        return (
            
            <div className="social">
                <a href="https://github.com/naaficodes" target="_blank"><i class="fab fa-github">github</i></a>
                <a href="https://Instagram.com/iam_naafi" target="_blank"><i class="fab fa-instagram">insta</i></a>
                <a href="https://www.facebook.com/abdul.w.naafi" target="_blank"><i class="fab fa-facebook-f">facebook</i></a>
                <a href="https://Linkedin.com/in/naafi" target="_blank"><i class="fab fa-linkedin-in">linkedin</i></a>
            </div>
            
        )
    }
}
export default Social
