import React, { Component } from 'react';
import Widecard from '../components/Widecard';
class Education extends Component {
    render() {
        return (
            <div className="condiv">
                <h1 className="subtopic">My Education</h1>
                <Widecard title="B.Tech CSE" where="BVRIT" from="2012" to="2016" />
                <Widecard title="Intermediate" where="Sree Medha Siddi jr College" from="2010" to="2012" />
                <Widecard title="School" where="Sai Grace Grammar High School" from="2000" to="2010" />
            </div>
        )
    }
}
export default Education