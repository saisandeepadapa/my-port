
import React, { Component } from 'react';
import ReactTypingEffect from 'react-typing-effect';
import sai from '../img/sai.png';
import Social from '../components/Social'
class Home extends Component {
    render() {
        return (
            <div className="condiv home">
                <img src={sai} className="profilepic"></img>
                <ReactTypingEffect className="typingeffect" text={['I am Sai Sandeep Adapa', 'I am a Full Stack developer']} speed={100} eraseDelay={700} />
                
            </div>
        )
    }
}
export default Home